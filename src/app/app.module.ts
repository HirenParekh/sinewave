import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import {SineWaveComponent} from './sine-wave/sine-wave.component';

@NgModule({
  declarations: [
    AppComponent,
    SineWaveComponent
  ],
  imports: [
    BrowserModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
