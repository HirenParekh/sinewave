import {AfterViewInit, Component} from '@angular/core';
import * as SineWaves from 'sine-waves';

@Component({
  selector: 'app-sine-wave',
  templateUrl: 'sine-wave.component.html',
  styleUrls: ['sine-wave.component.css']
})
export class SineWaveComponent implements AfterViewInit {
  waves: SineWaves;

  constructor() {

  }

  ngAfterViewInit() {
    this.initSineWave();
  }

  initSineWave() {
    this.waves = new SineWaves({
      el: document.getElementById('appSineWave'),
      speed: 4,

      ease: 'SineInOut',

      wavesWidth: '95%',

      waves: [
        {
          timeModifier: 1,
          lineWidth: 3,
          amplitude: 150,
          wavelength: 200,
          segmentLength: 20
        },
        {
          timeModifier: 1,
          lineWidth: 2,
          amplitude: 150,
          wavelength: 100
        },
        {
          timeModifier: 1,
          lineWidth: 1,
          amplitude: -150,
          wavelength: 50,
          segmentLength: 10
        },
        {
          timeModifier: 1,
          lineWidth: 0.5,
          amplitude: -100,
          wavelength: 100,
          segmentLength: 10
        }
      ],

      // Called on window resize
      resizeEvent: function () {
      }
    });
  }

}
